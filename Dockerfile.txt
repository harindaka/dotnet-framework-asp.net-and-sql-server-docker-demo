FROM mcr.microsoft.com/dotnet/framework/sdk:4.8 AS build-env

WORKDIR /app
COPY . .

RUN msbuild.exe /t:Build /p:Configuration=Release /p:OutputPath=out

FROM mcr.microsoft.com/dotnet/framework/runtime:4.8
WORKDIR /app
COPY --from=build-env /app/out ./
ENTRYPOINT ["dotnet", "WebApplication1.dll"]