# Dotnet Framework ASP.Net and SQL Server Docker Demo

### Prerequisites
1. A windows host running Windows Server 2016 or Windows 10 (Professional / Enterprise - Anniversary Update or later) with Docker for Desktop installed
2. Right click Docker Desktop icon in system tray and click "Switch to Windows Containers"
3. Since the base containers used are considerably large in size, you may want to change the docker program data directory (C:\ProgramData\Docker by default) to a drive with sufficient disk space (around 50GB). To do this, right click the Docker Desktop icon in system tray and click "Settings". Under the Daemon section on the left, change the "Basic" toggle to "Advanced". Add the following JSON key value pair to the existing JSON config and click "Apply". The value in this key pair can point to any preferred directory on a drive with sufficient space (in this case E:/DockerData).
```
"graph": "E:/DockerData"
```


### Instructions
1. Clone this repo
2. In Command Prompt, CD to the cloned directory
3. Execute the following commands

`webapp-build-image.bat` - Builds the WebApplication1 project and compiles a docker image named web-app-image. Note that the first time build may take a while due to docker downloading base images

`webapp-create-container.bat` - Creates a container named web-app-container using the docker image

`webapp-start-container.bat` - Starts the web-app-container created previously. You should be able to access the WebApplication1 via [http://localhost:8000](http://localhost:8000)

`webapp-stop-container.bat` and `webapp-delete-container.bat` - Self explanatory

### Notes
The source code and Dockerfile builds against .Net Framework 4.8. You may change the minor framework version in the Dockerfile and project files to target different 4.x versions and hopefully it should work (not tested).



