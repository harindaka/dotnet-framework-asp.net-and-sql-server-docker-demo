if not exists 
    (select name  
     from master.sys.server_principals
     where name = 'ApplicationLogin')
begin
    create login [ApplicationLogin] with password = N'Password@123'
end

if not exists
    (select name
     from sys.database_principals
     where name = 'ApplicationUser')
begin
    create USER [ApplicationUser] FOR login [ApplicationLogin];
end

if not exists (select top 1 [name] from sys.database_principals where name='ApplicationRole' and Type = 'R')
begin
    create role ApplicationRole;
end

grant SELECT, INSERT, UPDATE, DELETE, EXECUTE, CONTROL, ALTER on SCHEMA::DBO TO ApplicationRole;
grant CREATE DATABASE to ApplicationRole

alter role [ApplicationRole] add member [ApplicationUser];


