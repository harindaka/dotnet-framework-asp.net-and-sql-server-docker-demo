﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Link> Links { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringConfig = ConfigurationManager.ConnectionStrings["ApplicationDbContainer"];

            optionsBuilder.UseSqlServer(connectionStringConfig.ConnectionString);
        }
    } 
    
    public class Link
    {
        public int LinkId { get; set; }

        public string Url { get; set; }
        public string Name { get; set; }

    }
}