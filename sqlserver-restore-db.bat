@echo off
if [%1]==[] goto usage

set BACKUP_FILE_PATH=%1
docker exec -it sqlserver-container /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Password@123 -Q "DECLARE @BKP_FILE nvarchar(1000); SELECT @BKP_FILE = '/opt/sqlbackups/%BACKUP_FILE_PATH%'; RESTORE DATABASE [ApplicationDb] FROM DISK = @BKP_FILE WITH REPLACE"

goto :eof
:usage
@echo Usage: %0 ^<Backup File Name^>
exit /B 1