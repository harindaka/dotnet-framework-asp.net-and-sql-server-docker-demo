@echo off
docker logout
call sqlserver-delete-container.bat
docker run -it --name sqlserver-container -v sqlserver-volume:/var/opt/mssql -v e:/SqlBackups:/opt/sqlbackups -p 1433:1433 -e MSSQL_PID=Express -e SA_PASSWORD=Password@123 -e ACCEPT_EULA=Y --restart unless-stopped mcr.microsoft.com/mssql/server:2017-latest-ubuntu