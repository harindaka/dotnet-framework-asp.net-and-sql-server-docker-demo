@echo off
docker logout
call webapp-delete-container.bat
docker run -it --name webapp-container -p 8000:80 --restart unless-stopped webapp-image